import Foundation
import UIKit

class StartupScreenViewModel {
    
    var cityArray = [City]()
    var filteredData = [City]()
    var searchActive: Bool = false

    
    init() {
        self.cityArray.reserveCapacity(209579)
        self.filteredData.reserveCapacity(209579)
    }
    
    func openJSON() {
        if let path = Bundle.main.path(forResource: "city.list", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                if let jsonResult = jsonResult as? [[String: Any]] {
                    
                    for (key, _) in jsonResult.enumerated() {
                        if let allCities = jsonResult[key] as? [String: Any],
                            let id = allCities["id"] as? Int,
                            let name = allCities["name"] as? String,
                            let state = allCities["state"] as? String,
                            let country = allCities["country"] as? String,
                            let coord = allCities["coord"] as? [String: Any],
                            let lon = coord["lon"] as? Double,
                            let lat = coord["lat"] as? Double {
                            
                            let city = City(id: id, name: name, state: state, country: country, lon: lon, lat: lat)
                            cityArray.append(city)
                            filteredData.append(city)
                        }
                    }
                    cityArray.sort { (firstCity, secondCity) -> Bool in
                        return firstCity.country < secondCity.country
                    }
                    filteredData.sort { (firstCity, secondCity) -> Bool in
                        return firstCity.country < secondCity.country
                    }
                    print(jsonResult.count)
                    print(cityArray.count)
                    print()
                }
            } catch let error {
                print(error.localizedDescription)
            }
        }
    }
    
    func setupSearchBar(searchBar: UISearchBar, searchText: String, tableView: UITableView) {
        if searchBar.text == nil || searchBar.text == "" {
            searchActive = false
            tableView.reloadData()
        } else {
            searchActive = true
            self.filteredData = self.cityArray.filter({$0.name.range(of: searchText, options: .caseInsensitive, range: nil, locale: nil) != nil}) 
            tableView.reloadData()
        }
    }
    
    
    
    
    
    
    
}
