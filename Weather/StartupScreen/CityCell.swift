import UIKit

class CityCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configureCell(with object: City) {
        let cityString = "\(object.name)"
        let cityStringAttributes = [
            NSAttributedString.Key.foregroundColor:UIColor.white
        ]
        let attributedCityString = NSMutableAttributedString(string: cityString, attributes: cityStringAttributes)
        
        if object.state != "" {
            let stateString = " \(object.state)"
            let stateStringAttributes = [
                NSAttributedString.Key.foregroundColor: UIColor.lightGray
            ]
            let attributesStateString = NSAttributedString(string: stateString, attributes: stateStringAttributes)
            attributedCityString.append(attributesStateString)
        }
        
        if object.country != "" {
            let countryString = ", \(object.country)"
            let countryStringAttributes = [
                NSAttributedString.Key.foregroundColor: UIColor.lightGray
            ]
            let attributesCountryString = NSAttributedString(string: countryString, attributes: countryStringAttributes)
            attributedCityString.append(attributesCountryString)
        }
        
        textLabel?.attributedText = attributedCityString
    }

}
