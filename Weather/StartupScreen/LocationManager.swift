import Foundation
import CoreLocation
import UIKit

class LocationManager: CLLocationManager, CLLocationManagerDelegate {
    
    var finalLocation = CLLocation()
    var locationCoordinate = CLLocationCoordinate2D()
    
    override init() {
        super.init()
        self.delegate = self
        self.desiredAccuracy = kCLLocationAccuracyBest
        self.distanceFilter = 50
        self.activityType = .automotiveNavigation
        self.requestAlwaysAuthorization()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location: CLLocationCoordinate2D = manager.location?.coordinate {
            let finalLocation = CLLocation(latitude: location.latitude, longitude: location.longitude)
            self.locationCoordinate = location
            self.finalLocation = finalLocation
        }
        manager.stopUpdatingLocation()
        manager.delegate = nil
        NotificationCenter.default.post(name: NSNotification.Name.locationNotification, object: nil)
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            manager.requestLocation()
        case .authorizedAlways, .authorizedWhenInUse:
            manager.startUpdatingLocation()
            print("Permission granted")
        default:
            print("Permission denied")
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("LocationManager received an error")
    }
    
    
    
    
    
}

extension CLLocation {
    func fetchCityAndCountry(completion: @escaping (_ city: String?, _ state: String?, _ country:  String?, _ error: Error?) -> ()) {
        CLGeocoder().reverseGeocodeLocation(self) { completion($0?.first?.locality, $0?.first?.administrativeArea, $0?.first?.country, $1) }
    }
}
