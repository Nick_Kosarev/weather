import UIKit
import CoreLocation

class StartupScreen: UIViewController {
    
//MARK: - IBOutlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
//MARK: - Constants and variables
    var viewModel: StartupScreenViewModel?
    var locationManager: LocationManager?
    
//MARK: - Lifecycle functions
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewModel = StartupScreenViewModel()
        self.locationManager = LocationManager()
        self.viewModel?.openJSON()
        
        NotificationCenter.default.addObserver(self, selector: #selector(locationNotificationReceived), name: Notification.Name.locationNotification, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
//MARK: - IBActions
    @IBAction func locationNotificationReceived() {
        print("Notification received")
        self.instantiateViewController()
    }
    
//MARK: - Flow functions
    func instantiateViewController() {
        let locationCoordinate = locationManager?.locationCoordinate
        let finalLocation = locationManager?.finalLocation
        finalLocation?.fetchCityAndCountry { city, state, country, error in
            guard let city = city, let state = state, let country = country, error == nil else { return }
            if let controller = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as? ViewController {
                controller.name = city
                controller.state = state
                controller.country = country
                controller.lon = locationCoordinate?.longitude
                controller.lat = locationCoordinate?.latitude
                self.navigationController?.pushViewController(controller, animated: true)
            }
        }
    }
    
}

//MARK: - Extensions

extension StartupScreen: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.viewModel?.filteredData.count ?? Int()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? CityCell else { return UITableViewCell() }
        if let filteredData = self.viewModel?.filteredData {
            cell.configureCell(with: filteredData[indexPath.row])
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as? ViewController {
            let city = self.viewModel?.filteredData[indexPath.row]
            controller.id = city?.id
            controller.name = city?.name
            controller.state = city?.state
            controller.country = city?.country
            controller.lon = city?.lon
            controller.lat = city?.lat
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
}



extension StartupScreen: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.viewModel?.setupSearchBar(searchBar: searchBar, searchText: searchText, tableView: self.tableView)
    }
}

extension Notification.Name {
    static let locationNotification = Notification.Name("locationNotification")
}
