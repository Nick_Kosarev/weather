import Foundation

class City {
    
    var id: Int
    var name: String
    var state: String
    var country: String
    var lon: Double
    var lat: Double
    
    init(id: Int, name: String, state: String, country: String, lon: Double, lat: Double) {
        self.id = id
        self.name = name
        self.state = state
        self.country = country
        self.lon = lon
        self.lat = lat
        
    }
    
    
    
    
    
}
