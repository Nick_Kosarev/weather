import Foundation
import UIKit

class ViewModel {
    
    var id: Int?
    var name = Bindable<String?>(nil)
    var state: String?
    var country: String?
    var lon: Double?
    var lat: Double?
    var additionalCellArray = [AdditionalCellClass]()
    var dailyWeatherArray = [DailyWeather]()
    var hourlyWeatherArray = [HourlyWeather]()
    var currentTimeZoneOffset: Int?
    
    var currentWeather = Bindable<String?>(nil)
    var currentTemperature = Bindable<String?>(nil)
    var currentDay = Bindable<String?>(nil)
    var currentDayTemp = Bindable<String?>(nil)
    var currentNightTemp = Bindable<String?>(nil)
    
    
    init() {
        additionalCellArray.reserveCapacity(9)
        dailyWeatherArray.reserveCapacity(8)
        hourlyWeatherArray.reserveCapacity(48)
    }
    
    func getRequest() { self.sendRequest(endpoint: "/onecall?lat=\(lat!)&lon=\(lon!)&units=metric&exclude=minutely&appid=f729a237df2e470b8d1538b70f69284d") { (data) in
        do {
            let json = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
            if let jsonResult = json as? [String: Any],
                let daily = jsonResult["daily"] as? [[String: Any]],
                let hourly = jsonResult["hourly"] as? [[String:Any]] {
                let timeZone = jsonResult["timezone"] as? String
                let timeZoneOffset =  jsonResult["timezone_offset"] as? Int
                self.currentTimeZoneOffset = timeZoneOffset
                
                if let current = jsonResult["current"] as? [String: Any],
                    let clouds = current["clouds"] as? Int,
                    let currentTime = current["dt"] as? Double,
                    let feelsLike = current["feels_like"] as? Double,
                    let humidity = current["humidity"] as? Int,
                    let pressure = current["pressure"] as? Int,
                    let sunrise = current["sunrise"] as? Double,
                    let sunset = current["sunset"] as? Double,
                    let temperature = current["temp"] as? Double,
                    let uvi = current["uvi"] as? Double,
                    let weatherArray = current["weather"] as? [[String: Any]],
                    let weatherElement = weatherArray[0] as? [String: Any],
                    let weather = weatherElement["main"] as? String,
                    let icon = weatherElement["icon"] as? String,
                    let windDegree = current["wind_deg"] as? Int,
                    let windSpeed = current["wind_speed"] as? Double {
                    let visibility = current["visibility"] as? Int ?? 0
                    
                    let clouds = AdditionalCellClass(title: "Clouds ".localized(), value: ("\(String(clouds))%"))
                    let humidity = AdditionalCellClass(title: "Humidity ".localized(), value: ("\(String(humidity))%"))
                    let meters: String = "m".localized()
                    let visibilityInMeters = AdditionalCellClass(title: "Visibility ".localized(), value: ("\(visibility)\(meters)"))
                    let pascalsString: String = "hPa".localized()
                    let pressure = AdditionalCellClass(title: "Pressure ".localized(), value: ("\(String(pressure))\(pascalsString)"))
                    let sunrise = AdditionalCellClass(title: "Sunrise ".localized(), value: self.changeTimeFormat(time: sunrise, format: "HH:mm"))
                    let sunset = AdditionalCellClass(title: "Sunset ".localized(), value: self.changeTimeFormat(time: sunset, format: "HH:mm"))
                    let uvi = AdditionalCellClass(title: "UVI ".localized(), value: String(Int(uvi)))
                    let windDegree = AdditionalCellClass(title: "Wind degree ".localized(), value: ("\(String(windDegree))°"))
                    let metersPerSecString: String = "m/s".localized()
                    let windSpeed = AdditionalCellClass(title: "Wind speed ".localized(), value: ("\(String(Int(windSpeed)))\(metersPerSecString)"))
                    
                    self.additionalCellArray.append(contentsOf: [clouds, humidity, visibilityInMeters, pressure, sunrise, sunset, uvi, windDegree, windSpeed])
                    
                    self.currentWeather.value = weather.localized()
                    self.currentTemperature.value = String("\(Int(temperature))°")
                    
                }
                
                for (key, _) in daily.enumerated() {
                    if let allDays = daily[key] as? [String: Any],
                        let clouds = allDays["clouds"] as? Int,
                        let time = allDays["dt"] as? Double,
                        let feelsLikeElement = allDays["feels_like"] as? [String: Any],
                        let feelsLikeDay = feelsLikeElement["day"] as? Double,
                        let feelsLikeEve = feelsLikeElement["eve"] as? Double,
                        let feelsLikeMorning = feelsLikeElement["morn"] as? Double,
                        let feelsLikeNight = feelsLikeElement["night"] as? Double,
                        let humidity = allDays["humidity"] as? Int,
                        let pressure = allDays["pressure"] as? Int,
                        let sunrise = allDays["sunrise"] as? Int,
                        let sunset = allDays["sunset"] as? Int,
                        let tempElement = allDays["temp"] as? [String: Any],
                        let tempDay = tempElement["day"] as? Double,
                        let tempEve = tempElement["eve"] as? Double,
                        let tempMax = tempElement["max"] as? Double,
                        let tempMin = tempElement["min"] as? Double,
                        let tempMorn = tempElement["morn"] as? Double,
                        let tempNight = tempElement["night"] as? Double,
                        let uvi = allDays["uvi"] as? Double,
                        let weatherArray = allDays["weather"] as? [[String: Any]],
                        let weatherElement = weatherArray[0] as? [String: Any],
                        let weatherTitle = weatherElement["main"] as? String,
                        let weatherIcon = weatherElement["icon"] as? String,
                        let windDegree = allDays["wind_deg"] as? Int,
                        let windSpeed = allDays["wind_speed"] as? Double {
                        let dailyWeather = DailyWeather(clouds: clouds, time: time, feelsLikeDay: feelsLikeDay, feelsLikeEve: feelsLikeEve, feelsLikeMorning: feelsLikeMorning, feelsLikeNight: feelsLikeNight, tempDay: tempDay, tempEve: tempEve, tempMax: tempMax, tempMin: tempMin, tempMorn: tempMorn, tempNight: tempNight, humidity: humidity, pressure: pressure, sunrise: sunrise, sunset: sunset, uvi: uvi, weatherTitle: weatherTitle, weatherIcon: weatherIcon, windDegree: windDegree, windSpeed: windSpeed, timeZoneOffset: timeZoneOffset ?? 0)
                        
                        self.dailyWeatherArray.append(dailyWeather)
                        self.dailyWeatherArray.count
                    }
                }
                
                for(key, _) in hourly.enumerated() {
                    if let allHours = hourly[key] as? [String: Any],
                        let clouds = allHours["clouds"] as? Int,
                        let time = allHours["dt"] as? Double,
                        let feelsLike = allHours["feels_like"] as? Double,
                        let humidity = allHours["humidity"] as? Int,
                        let pressure = allHours["pressure"] as? Int,
                        let temp = allHours["temp"] as? Double,
                        let weatherArray = allHours["weather"] as? [[String: Any]],
                        let weatherElement = weatherArray[0] as? [String: Any],
                        let weatherIcon = weatherElement["icon"] as? String,
                        let weatherTitle = weatherElement["main"] as? String,
                        let windDegree = allHours["wind_deg"] as? Int,
                        let windSpeed = allHours["wind_speed"] as? Double {
                        let hourlyWeather = HourlyWeather(clouds: clouds, time: time, feelsLike: feelsLike, humidity: humidity, pressure: pressure, temp: temp, weatherIcon: weatherIcon, weatherTitle: weatherTitle, windDegree: windDegree, windSpeed: windSpeed, timeZoneOffset: timeZoneOffset ?? 0)
                        
                        self.hourlyWeatherArray.append(hourlyWeather)
                    }
                }
                self.assignCurrentWeather()
            }
            print()
        } catch let error {
            print(error.localizedDescription)
        }
        }
    }
    
    func assignCurrentWeather() {
        let todaysWeather = dailyWeatherArray[0]
        let currentDayString = self.changeTimeFormat(time: todaysWeather.time, format: "E, MMM d")
        let currentDayTempString = ("\(String(Int(todaysWeather.tempDay)))°")
        let currentNightTempString = ("\(String(Int(todaysWeather.tempNight)))°")
        currentDay.value = currentDayString
        currentDayTemp.value = currentDayTempString
        currentNightTemp.value = currentNightTempString
    }
    
    private func changeTimeFormat(time: Double, format: String) -> String {
        let date = Date(timeIntervalSince1970: time)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.timeZone = TimeZone(secondsFromGMT: currentTimeZoneOffset ?? 0)
        let localTime = dateFormatter.string(from: date).capitalized
        return localTime
    }
    
    
    private func sendRequest(endpoint: String, completion: @escaping (Data)->()) {
        guard let url = URL(string: "https://api.openweathermap.org/data/2.5\(endpoint)") else {
            return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if error == nil, let data = data {
                completion(data)
            } else {
                print(error?.localizedDescription)
            }
        }
        task.resume()
    }
    
    func gradientBackground() -> ([UIColor], [Double]) {
        let hour = Calendar.current.component(.hour, from: Date())
        print(hour)
        let midnight: [Double] = [0.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0]
        let earlyMorning: [Double] = [0.0, 1.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0]
        let morning: [Double] = [0.0, 0.0, 0.4, 0.8, 1.2, 1.6, 2.0, 2.0, 2.0]
        let lateMorning: [Double] = [0.0, 0.0, 0.0, 0.0, 0.2, 0.6, 1.0, 1.2, 2.0]
        let midday: [Double] = [0.0, 0.4, 1.0, 1.5, 2.0, 2.0, 2.0, 2.0, 2.0]
        let earlyEvening: [Double] = [0.0, 0.0, 0.0, 0.6, 1.0, 1.5, 2.0, 2.0, 2.0]
        let evening: [Double] = [0.0, 0.0, 0.0, 0.0, 0.0, 0.4, 1.0, 1.5, 2.0]
        let lateEvening: [Double] = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.3, 0.7, 1.0]
        let dayArray: [UIColor] = [.red, .orange, .yellow, .orange, .red, .systemPink, .purple, .indigoColor, .black]
        let nightArray: [UIColor] = [.black, .indigoColor, .purple, .systemPink, .red, .orange, .yellow, .orange, .black]
        switch hour {
        case 0..<3:
            return (nightArray, midnight)
        case 3..<6:
            return (nightArray, earlyMorning)
        case 6..<9:
            return (nightArray, morning)
        case 9..<12:
            return (nightArray, lateMorning)
        case 12..<15:
            return (dayArray, midday)
        case 15..<18:
            return (dayArray, earlyEvening)
        case 18..<21:
            return (dayArray, evening)
        case 21..<24:
            return (dayArray, lateEvening)
        default:
            return ([], [])
        }
    }
    
    func setupTableSections(section: Int) -> Int {
        switch section {
        case 0:
            return dailyWeatherArray.count
        case 1:
            return additionalCellArray.count
        default:
            return 0
        }
    }
    
    
}
