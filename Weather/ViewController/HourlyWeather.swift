import Foundation

class HourlyWeather {
    
    var clouds: Int
    var time: Double
    var feelsLike: Double
    var humidity: Int
    var pressure: Int
    var temp: Double
    var weatherIcon: String
    var weatherTitle: String
    var windDegree: Int
    var windSpeed: Double
    var timeZoneOffset: Int
    
    init(clouds: Int, time: Double, feelsLike: Double, humidity: Int, pressure: Int, temp: Double, weatherIcon: String, weatherTitle: String, windDegree: Int, windSpeed: Double, timeZoneOffset: Int) {
        
        self.clouds = clouds
        self.time = time
        self.feelsLike = feelsLike
        self.humidity = humidity
        self.pressure = pressure
        self.temp = temp
        self.weatherIcon = weatherIcon
        self.weatherTitle = weatherTitle
        self.windDegree = windDegree
        self.windSpeed = windSpeed
        self.timeZoneOffset = timeZoneOffset
    }
    
}
