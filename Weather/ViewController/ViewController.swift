import UIKit

class ViewController: UIViewController {
    
//MARK: - IBOutlets
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var currentWeatherLabel: UILabel!
    @IBOutlet weak var currentTemperatureLabel: UILabel!
    @IBOutlet weak var currentDayLabel: UILabel!
    @IBOutlet weak var currentDayTempLabel: UILabel!
    @IBOutlet weak var currentNightTempLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
//MARK: - Constants and variables
    var id: Int?
    var name: String?
    var state: String?
    var country: String?
    var lon: Double?
    var lat: Double?
    var viewModel: ViewModel?
    let spinnerView = UIView()
    
//MARK: - Lifecycle functions
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewModel = ViewModel()
        self.passData()
        let backgroundTuple = self.viewModel?.gradientBackground()
        self.view.applyGradient(colours: backgroundTuple?.0 ?? [], locations: backgroundTuple?.1 as [NSNumber]?)
        self.spinner.isHidden = false
        self.viewModel?.getRequest()
        self.spinnerBackgroundCheck()
    }
    
    override func viewDidLayoutSubviews() {
        self.bindOutlets()
    }
    
    //override func viewDidAppear(_ animated: Bool) {
    //    self.bindOutlets()
    //}
    
    
    
//MARK: - IBActions
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
//MARK: - Flow functions
    private func passData() {
        self.viewModel?.id = self.id
        self.viewModel?.name.value = self.name
        self.viewModel?.state = self.state
        self.viewModel?.country = self.country
        self.viewModel?.lon = self.lon
        self.viewModel?.lat = self.lat
    }
    
    private func bindOutlets() {
        self.viewModel?.name.bind({ (name) in
            DispatchQueue.main.async {
                self.cityLabel.text = name
            }
        })
        self.viewModel?.currentWeather.bind({ (currentWeather) in
            DispatchQueue.main.async {
                self.currentWeatherLabel.text = currentWeather
            }
        })
        self.viewModel?.currentTemperature.bind({ (currentTemperature) in
            DispatchQueue.main.async {
                self.currentTemperatureLabel.text = currentTemperature
            }
        })
        self.viewModel?.currentDay.bind({ (currentDay) in
            DispatchQueue.main.async {
                self.currentDayLabel.text = currentDay
            }
        })
        self.viewModel?.currentDayTemp.bind({ (currentDayTemp) in
            DispatchQueue.main.async {
                self.currentDayTempLabel.text = currentDayTemp
            }
        })
        self.viewModel?.currentNightTemp.bind({ (currentNightTemp) in
            DispatchQueue.main.async {
                self.currentNightTempLabel.text = currentNightTemp
            }
        })
        self.tableView.reloadData()
        self.collectionView.reloadData()
        self.spinner.isHidden = true
        self.spinnerBackgroundCheck()
    }
    
    func spinnerBackgroundCheck() {
        spinnerView.frame.size = self.view.frame.size
        spinnerView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        if self.spinner.isHidden == false {
            self.view.addSubview(spinnerView)
            self.view.bringSubviewToFront(spinner)
        } else {
            spinnerView.removeFromSuperview()
        }
    }
}

//MARK: - Extensions
extension ViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.viewModel?.setupTableSections(section: section) ?? Int()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case 0:
            guard let dailyCell = tableView.dequeueReusableCell(withIdentifier: "daily", for: indexPath) as? DailyCell else { return UITableViewCell() }
            if let dailyWeatherArray = self.viewModel?.dailyWeatherArray {
                dailyCell.configureCell(with: dailyWeatherArray[indexPath.row])
            }
            return dailyCell
        case 1:
            guard let additionalCell = tableView.dequeueReusableCell(withIdentifier: "additional", for: indexPath) as? AdditionalCell else { return UITableViewCell() }
            if let additionalCellArray = self.viewModel?.additionalCellArray {
                additionalCell.configureCell(with: additionalCellArray[indexPath.row])
            }
            return additionalCell
        default:
            return UITableViewCell()
        }
    }
}

extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.viewModel?.hourlyWeatherArray.count ?? Int()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let hourlyCell = collectionView.dequeueReusableCell(withReuseIdentifier: "hourly", for: indexPath) as? HourlyCell else { return UICollectionViewCell() }
        if let hourlyWeatherArray = self.viewModel?.hourlyWeatherArray {
            hourlyCell.configureCell(with: hourlyWeatherArray[indexPath.row])
        }
        return hourlyCell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let height = self.collectionView.frame.size.height
        let width = self.collectionView.frame.size.height * 0.75
        return CGSize(width: width, height: height)
    }
    
    
    
}

