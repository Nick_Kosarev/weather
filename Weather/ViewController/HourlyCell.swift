import UIKit

class HourlyCell: UICollectionViewCell {
    
    @IBOutlet weak var hourLabel: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var weatherIcon: UIImageView!
    
    func configureCell(with object: HourlyWeather) {
        guard let url = URL(string: "https://openweathermap.org/img/wn/\(object.weatherIcon).png") else { return }
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if error == nil, let data = data {
                DispatchQueue.main.async() { [weak self] in
                    self?.weatherIcon.image = UIImage(data: data)
                }
            }
        }
        task.resume()
        
        let date = Date(timeIntervalSince1970: object.time)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        dateFormatter.timeZone = TimeZone(secondsFromGMT: object.timeZoneOffset)
        let localTime = dateFormatter.string(from: date)
        hourLabel.text = localTime
        tempLabel.text = "\(String(Int(object.temp)))°"
    }
    
    
    
}
