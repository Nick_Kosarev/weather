import Foundation

class DailyWeather {
    
    var clouds: Int
    var time: Double
    var feelsLikeDay: Double
    var feelsLikeEve: Double
    var feelsLikeMorning: Double
    var feelsLikeNight: Double
    var tempDay: Double
    var tempEve: Double
    var tempMax: Double
    var tempMin: Double
    var tempMorn: Double
    var tempNight: Double
    var humidity: Int
    var pressure: Int
    var sunrise: Int
    var sunset: Int
    var uvi: Double
    var weatherTitle: String
    var weatherIcon: String
    var windDegree: Int
    var windSpeed: Double
    var timeZoneOffset: Int
    
    
    
    init(clouds: Int, time: Double, feelsLikeDay: Double, feelsLikeEve: Double, feelsLikeMorning: Double, feelsLikeNight: Double, tempDay: Double, tempEve: Double, tempMax: Double, tempMin: Double, tempMorn: Double, tempNight: Double, humidity: Int, pressure: Int, sunrise: Int, sunset: Int, uvi: Double, weatherTitle: String, weatherIcon: String, windDegree: Int, windSpeed: Double, timeZoneOffset: Int) {
        self.clouds = clouds
        self.time = time
        self.feelsLikeDay = feelsLikeDay
        self.feelsLikeEve = feelsLikeEve
        self.feelsLikeMorning = feelsLikeMorning
        self.feelsLikeNight = feelsLikeNight
        self.tempDay = tempDay
        self.tempEve = tempEve
        self.tempMax = tempMax
        self.tempMin = tempMin
        self.tempMorn = tempMorn
        self.tempNight = tempNight
        self.humidity = humidity
        self.pressure = pressure
        self.sunrise = sunrise
        self.sunset = sunset
        self.uvi = uvi
        self.weatherTitle = weatherTitle
        self.weatherIcon = weatherIcon
        self.windDegree = windDegree
        self.windSpeed = windSpeed
        self.timeZoneOffset = timeZoneOffset
    }
    
    
}
