import UIKit

class DailyCell: UITableViewCell {
    
    @IBOutlet weak var day: UILabel!
    @IBOutlet weak var tempDay: UILabel!
    @IBOutlet weak var tempNight: UILabel!
    @IBOutlet weak var weatherIcon: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configureCell(with object: DailyWeather) {
        guard let url = URL(string: "https://openweathermap.org/img/wn/\(object.weatherIcon).png") else { return }
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if error == nil, let data = data {
                DispatchQueue.main.async() { [weak self] in
                    self?.weatherIcon.image = UIImage(data: data)
                }
            }
        }
        task.resume()
        
        let date = Date(timeIntervalSince1970: object.time)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "E, MMM d"
        dateFormatter.timeZone = TimeZone(secondsFromGMT: object.timeZoneOffset)
        let localDate = dateFormatter.string(from: date).capitalized
        
        day.text = String(localDate)
        tempDay.text = "\(String(Int(object.tempDay)))°"
        tempNight.text = "\(String(Int(object.tempNight)))°"
    }
}
